<?php

/**
 * Gestion de la tache périodique de nettoyage des cartes postales.
 *
 * @package SPIP\CartePostale\Genie
**/

// sécurité
if (!defined("_ECRIRE_INC_VERSION")) return;

/**
 * Nettoyage des cartes postales périmées
 *
 * Toutes les cartes postales agées de plus de x jours (défini en config)
 * sont supprimées du répertoire de stockage des cartes postales calculées.
 * 
 * @param int $t
 *     Timestamp du dernier appel de la tâche
 * @return int
 *     1 : la tâche est effectuée
**/
function genie_nettoyer_cartes_postales_dist($t){
	include_spip('inc/config');

	// nombre de jours de conservation
	$age = intval(lire_config('carte_postale/suppression_vieilles_cartes', 30));
	spip_log("Purge des cartes postales agees de $age jours");
	$age = time() - ($age * 24 * 3600);

	include_spip('inc/invalideur');
	purger_repertoire(_DIR_IMG . 'cartes_postales/', array(
		'atime' => $age,
	));

	return 1;
}
