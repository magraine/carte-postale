<?php

/**
 * Utilisation de pipelines
 * 
 * @package SPIP\CartePostale\Pipelines
**/

// sécurité
if (!defined("_ECRIRE_INC_VERSION")) return;

/**
 * Déclarer le génie de nettoyage des cartes postales
 *
 * @param array $taches
 *      Couples (nom de la tâche => période en seconde)
 * @return array
 *      Couples (nom de la tâche => période en seconde)
**/
function carte_postale_taches_generales_cron($taches){
	$taches['nettoyer_cartes_postales'] = 7*24*3600; // toutes les semaines
	return $taches;
}

