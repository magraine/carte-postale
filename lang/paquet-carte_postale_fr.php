<?php

// Ceci est un fichier langue de SPIP -- This is a SPIP language file

///  Fichier produit par PlugOnet
// Module: paquet-carte_postale
// Langue: fr
// Date: 14-09-2012 18:42:30
// Items: 2

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

// C
	'carte_postale_description' => 'Proposer d\'envoyer des cartes postales
		depuis votre site !',
	'carte_postale_slogan' => 'Proposer d\'envoyer des cartes postales',
);
?>