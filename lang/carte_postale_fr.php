<?php

$GLOBALS[$GLOBALS['idx_lang']] = array(
	// cfg
	"cfg_titre_carte_postale" => "Carte Postale",
	"cfg_descriptif_carte_postale" => "Configuration
		vous permettant d'optimiser certains calculs pour acc&eacute;l&eacute;ler la
		cr&eacute;ation de la carte postale.",
	
	// formulaire
	"legend_carte" => "La carte et son texte",
	"legend_destinataire" => "Destinataire",
	"legend_expediteur" => "Expediteur",

	"label_carte_postale" => "Choisir la carte postale",
	"label_titre" => "Titre",
	"label_texte" => "Texte",
	"label_nom_destinataire" => "Nom",
	"label_email_destinataire" => "Email",
	"label_nom_expediteur" => "Nom",
	"label_email_expediteur" => "Email",
	
	"label_couleur_credits" => "Couleur du cr&eacute;dit",
	"label_redimensionnement" => "Redimensionnement de la carte",
	"label_saisie_taille_selection" => "Largeur de la carte s&eacute;lectionn&eacute;e",
	"label_saisie_taille_vignette" => "Largeur des vignettes &agrave; s&eacute;lectionner",
	"label_suppression_vieilles_cartes" => "P&eacute;remption des cartes",

	"lire_correctement" => "Vous pouvez <a href='@url@'>consulter la carte postale en ligne</a>
		si elle ne semble pas s'afficher correctement !",
	
	"envoyer" => "Envoyer la carte !",
	"envoi_carte_ok" => "La carte postale a &eacute;t&eacute; envoy&eacute;e !",
	"ecrire_nouvelle_carte" => "Voulez-vous <a href='@url@'>&eacute;crire une nouvelle carte ?</a>",
	"erreur_champ_obligatoire" => "Ce renseignement est obligatoire !",
	"erreur_email_incorrect" => "Cette adresse email ne semble pas valide !",

	"explication_couleur_credits" => "Vous pouvez d&eacute;finir une couleur
		pour l'ajout automatique des cr&eacute;dits sur en bas &agrave; droite de la carte,
		en son absence, une couleur, blanc ou noir est calcul&eacute;e automatiquement,
		mais n&eacute;cessite un temps de calcul sup&eacute;rieur. Exemple : #ffffff",

	"explication_redimensionnement" => "La carte est r&eacute;duite à 450px
		de largeur pour fonctionner dans l'interface priv&eacute;e.
		Vous ne pourrez pas d&eacute;passer une taille de 640px de large.
		Exemple : 450",

	"explication_suppression_vieilles_cartes" => "Les cartes postales envoy&eacute;es
		sont conserv&eacute;es sur le serveur un certain nombre de jours avant d'&ecirc;tre
		supprim&eacute;es. D&eacute;finir ici le nombre de jour avant la suppression des cartes.
		Exemple : 30",
);
?>
