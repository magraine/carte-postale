<?php

/**
 * Gestion du formulaire de création de carte postale
 * 
 * @package SPIP\CartePostale\Formulaires
**/

// sécurité
if (!defined("_ECRIRE_INC_VERSION")) return;

/**
 * Chargement du formulaire de création de carte postale
 *
 * @param int $id_article
 *     Identifiant de l'article qui contient les documents (images)
 *     servant à créer les cartes postales
 * @return array
 *     Environnement du formulaire
**/
function formulaires_carte_postale_charger_dist($id_article=0) {
	// pas d'article, pas de carte liee
	if (!$id_article = intval($id_article)) {
		return false;
	}

	return array(
		'id_article' => $id_article,
		'id_carte_postale' => '', // id_document
		'titre' => '',
		'texte' => '',
		'nom_destinataire' => '',
		'email_destinataire' => '',
		'nom_expediteur' => '',
		'email_expediteur' => '',
		'_pipeline' => array('editer_contenu_formulaire_carte_postale',
			'args' => array('id_article' => $id_article))
	);
}

/**
 * Vérifications du formulaire de création de carte postale
 *
 * Vérifie les champs obligatoires et les cohérence des emails.
 * Retourne une erreur factice si une prévisualisation est demandée.
 * 
 * @param int $id_article
 *     Identifiant de l'article qui contient les documents (images)
 *     servant à créer les cartes postales
 * @return array
 *     Tableau des erreurs
**/
function formulaires_carte_postale_verifier_dist($id_article=0) {
	$erreurs = array();

	// obligatoires ?
	foreach (array(
		'id_carte_postale',
		'titre', 'texte',
		'email_destinataire',
		'email_expediteur'
		) as $obli) {
			if (!_request($obli)) {
				$erreurs[$obli] = _T("carte_postale:erreur_champ_obligatoire");
			}
	}
	// mail selon RFC ?
	if (!isset($erreurs['email_destinataire'])
	and !(email_valide(_request('email_destinataire')))) {
		$erreurs['email_destinataire'] = _T("carte_postale:erreur_email_incorrect");
	}
	if (!isset($erreurs['email_expediteur'])
	and !(email_valide(_request('email_expediteur')))) {
		$erreurs['email_expediteur'] = _T("carte_postale:erreur_email_incorrect");
	}
		
	if (!$erreurs and _request('previsualiser')) {
		$erreurs['previsualiser'] = ' ';
	}
	
	return $erreurs;
}


/**
 * Traitements du formulaire de création de carte postale
 *
 * @see carte_postale_creer()
 * 
 * @param int $id_article
 *     Identifiant de l'article qui contient les documents (images)
 *     servant à créer les cartes postales
 * @return array
 *     Retour des traitements
**/
function formulaires_carte_postale_traiter_dist($id_article=0) {
	$res = array();
	$champs = array();
	
	// a traiter
	foreach (array(
		'id_carte_postale',
		'titre', 'texte',
		'nom_destinataire', 'email_destinataire',
		'nom_expediteur', 'email_expediteur',
		) as $champ) {
			$champs[$champ] = _request($champ);
	}
	$champs['id_article'] = $id_article;
	
	// creer la page, la sauver, envoyer le mail avec la page
	// et un lien pour y acceder aussi...
	carte_postale_creer($champs);
	
	$res["editable"] = false;
	$res["message_ok"] = _T("carte_postale:envoi_carte_ok");

	return $res;
}

/**
 * Stocke une carte postale à son emplacement définitif et envoi le mail
 * 
 * Crée un hash à partir des infos saisies par le créateur de
 * la carte, calcule le HTML de la carte postale, le sauvegarde, puis envoie
 * la carte postale par email avec un lien pour la visualiser
 *
 * @param array $contexte
 *     Contexte servant (saisies utilisateur) servant à calculer la carte
 * @return void
**/
function carte_postale_creer($contexte) {
	
	// calcul du html
	$hash = md5(implode('',$contexte));
	$contexte['hash'] = $hash;
	$html = recuperer_fond('carte_postale', $contexte);

	// sauver le fichier
	sous_repertoire(_DIR_IMG, 'cartes_postales');
	$sauver = _DIR_IMG . 'cartes_postales/' . $hash . '.html';
	ecrire_fichier($sauver, $html);

	$phrase = '<em>'
		. _T("carte_postale:lire_correctement", array(
			'url' => generer_url_public('carte_postale', 'id='.$hash)
			))
		. '</em>';
	$html = str_replace('<body>', '<body>' . $phrase, $html);
	
	// envoyer le mail
	// retransformer les &chose; dans le bon charset
	#include_spip('inc/charsets');
	#$titre = unicode2charset(html2unicode($titre));
	#$texte = unicode2charset(html2unicode($texte));
	$envoyer_mail = charger_fonction('envoyer_mail', 'inc');
	$envoyer_mail(
		$contexte['email_destinataire'],
		$contexte['titre'],
		array('html' => $html),
		$contexte['email_expediteur']
	);
}
?>
