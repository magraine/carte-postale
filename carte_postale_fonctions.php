<?php

/**
 * Filtres pour les squelettes utilisant une carte postale
 *
 * @package SPIP\CartePostale\Fonctions
**/

// sécurité
if (!defined("_ECRIRE_INC_VERSION")) return;

if (!function_exists('img_absolue')) {
/**
 * Remplace le lien (généralement relatif) de la source d'une image
 * par un lien absolu
 *
 * @param string $img
 *     Code HTML d'une balise <img>
 * @return string
 *     Code HTML d'une balise <img>
**/
function img_absolue($img){
	$src = extraire_attribut($img,'src');
	$src = url_absolue($src);
	$img = inserer_attribut($img,'src',$src);
	return $img;
}
}

/**
 * Copie l'image de carte postale calculée dans un emplacement
 * du répertoire IMG et utilise ce nouveau chemin pour l'image
 *
 * @param string $img
 *     Code HTML d'une balise <img>
 * @param int $id
 *     Identifiant du document ayant servi de source
 *     au calcul de la carte postale
 * @return string
 *     Code HTML d'une balise <img> avec son nouveau chemin
**/
function image_copier_carte_postale($img, $id){
	// extraire le nom du fichier et l'extension
	$fichier = extraire_attribut($img, 'src');
	if (($p=strpos($fichier,'?'))!==FALSE)
		$fichier=substr($fichier,0,$p);
	if (strlen($fichier) < 1)
		$fichier = $img;
	$ext = pathinfo($fichier);
	$ext = $ext['extension'];

	// calculer la destination et copier l'image si necessaire.
	$dir = _DIR_IMG . 'cartes_postales';
	sous_repertoire($dir);
	$hash = substr(md5($fichier), 0, 6);
	$new = "$dir/carte_$id" . "_$hash.$ext";
	if (!@file_exists($new)) {
		@copy($fichier, $new);
	}
	$img = inserer_attribut($img, 'src', $new);
	
	return $img; // on ne change rien (sauf l'url de sa source)
}
